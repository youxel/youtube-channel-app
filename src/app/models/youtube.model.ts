export enum ThumbnailType {
  default, high, medium
}

export class YoutubeSearchModel
{
 kind: string;
 etag: string;
 nextPageToken: string;
 prevPageToken: string;
 regionCode: string;
 pageInfo: {
   totalResults: number,
   resultsPerPage: number
  };
 items: YoutubeSearchItem[];
}

export class YoutubeSearchItem {
   kind: string;
   etag: string;
   id: {
     kind: string,
     videoId: string,
     channelId: string,
     playlistId: string
    };
   snippet: {
     publishedAt: Date,
     channelId: string,
     title: string,
     description: string,
     thumbnails: {
        default : {url: string, width: number, height: number},
        high : {url: string, width: number, height: number},
        medium : {url: string, width: number, height: number},
      };
     channelTitle: string,
     liveBroadcastContent: string
    };
  }

export class ChannelInfo {
  items: [
    {snippet: {
      thumbnails: {
       default : {url: string, width: number, height: number},
       high : {url: string, width: number, height: number},
       medium : {url: string, width: number, height: number},
      }
     }
    }
  ];
}

export class VideoInfo {
  "items": [
    {
      "kind": string,
      "etag": string,
      "id": string,
      "snippet": {
        "publishedAt": string,
        "channelId": string,
        "title": string,
        "description": string,
        "channelTitle": string,
        "tags": []
      };
      "contentDetails": {
        "duration": string,
        "dimension": string,
        "definition": string
      };
      "statistics": {
        "viewCount": string,
        "likeCount": string,
        "dislikeCount": string,
        "favoriteCount": string,
        "commentCount": string
      };
    }
  ];
}


