import { DbService } from './../../services/db.service';
import { YoutubeService } from './../../services/youtube.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { VideoInfo, YoutubeSearchItem } from 'src/app/models/youtube.model';
import { LoadingService } from 'src/app/services/loading.service';
import { DbModel } from 'src/app/models/db.model';
import { NbComponentStatus, NbGlobalPhysicalPosition, NbGlobalPosition, NbGlobalPositionStrategy, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  // Data Variables
  selectedVideo: YoutubeSearchItem;
  videoSources: Plyr.Source[] = [];
  videoID: string = this.actRoute.snapshot.params['id'];
  videoLocalData: DbModel;

  // Video Data
  videoDuration: string;
  videoTitle: string;
  videoDescription: string;
  videoViews: string;

  // UI Data Variables
  loadingVideo = false;

  // UI Variables
  ratingStarsStatus: boolean[] = new Array(5);

  constructor(
    public dbService: DbService,
    public youtubeService: YoutubeService,
    public loadingService: LoadingService,
    private toastrService: NbToastrService,
    private router: Router,
    private actRoute: ActivatedRoute,
    ) {
      this.ratingStarsStatus.fill(false);
      this.getVideoLocalData();
    }

  ngOnInit(): void {
    this.getYoutubeVideo();
    this.videoSources.push({src: this.videoID ,provider: 'youtube',});
  }

  // Get User Data
  async getYoutubeVideo() {
    this.loadingVideo = true;
    this.loadingService.showLoader();
    const videoData: VideoInfo = await this.youtubeService.getVideoInfo(this.videoID).toPromise();
    this.parseYoutubeDuration(videoData.items[0].contentDetails.duration);
    this.videoTitle = videoData.items[0].snippet.title;
    this.videoDescription = videoData.items[0].snippet.description;
    this.videoViews = videoData.items[0].statistics.viewCount;
    this.loadingVideo = false;
  }

  getVideoLocalData() {
    this.videoLocalData = this.dbService.getDbValueByID(this.videoID);
    if (!this.videoLocalData) {
      this.videoLocalData = {videoID: this.videoID, rate: 0, isFav: false};
    }
    this.ratingStarsStatus.fill(false);
    this.ratingStarsStatus.fill(true, 0 , this.videoLocalData.rate);
  }

  // User Actions Functions
  addVideoToFavorite() {
    this.dbService.saveDbValue({videoID: this.videoID, rate: 0, isFav: true}, 'fav');
    this.getVideoLocalData();
    this.showToast('success','Great!','This video is added to your favorite list');
  }

  removeVideoFromFavorite() {
    this.dbService.saveDbValue({videoID: this.videoID, rate: 0, isFav: false}, 'fav')
    this.getVideoLocalData();
    this.showToast('warning','Watchout!','This video was removed from your favorite list');
  }

  rateThisVideo(rate: number) {
    // if (!this.videoLocalData.rate){
      this.dbService.saveDbValue({videoID: this.videoID, rate: rate + 1, isFav: false} , 'rate')
      this.getVideoLocalData();
      this.showToast('primary','ThankYou!','Rated Video Successfully');
    // }
  }

  // Data Remodeling Functions
  parseYoutubeDuration(duration) {
    var total = 0;
    var hours = duration.match(/(\d+)H/);
    var minutes = duration.match(/(\d+)M/);
    var seconds = duration.match(/(\d+)S/);
    if (hours) total += parseInt(hours[1]) * 3600;
    if (minutes) total += parseInt(minutes[1]) * 60;
    if (seconds) total += parseInt(seconds[1]);

    this.videoDuration = new Date(total * 1000).toISOString().substr(11, 8);
    console.log(this.videoDuration)
  }

  // Generic Functions
  backToDashboard() {
    this.router.navigate(['dashboard'])
  }

  showToast(status: NbComponentStatus = 'primary', title: string = '', message: string = '', position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT) {
    this.toastrService.show(
      message,
      title,
      { position, status });
  }

}
