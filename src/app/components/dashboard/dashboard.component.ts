import { LoadingService } from './../../services/loading.service';
import { ChannelInfo, YoutubeSearchModel } from '../../models/youtube.model';
import { YoutubeService } from '../../services/youtube.service';
import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NbDialogService, NbListItemComponent, NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';

interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  Title: string;
  Preview: string;
  Published: string;
  Details: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  // Data Variables
  channelID = "UCpVm7bg6pXKo1Pr6k5kxG9A";
  youtubeResult: YoutubeSearchModel;
  public data: TreeNode<FSEntry>[];

  // Data Table Variables
  dataSource: NbTreeGridDataSource<FSEntry>;
  sortColumn: string;
  sortDirection: NbSortDirection = NbSortDirection.NONE;
  previewColumn = 'Preview';
  detailsColumn = 'Details';
  defaultColumns = ['Title', 'Published'];
  allColumns = [this.previewColumn, ...this.defaultColumns, this.detailsColumn];

  // Youtube Pagination Logic Variables
  nextPageToken: string;
  PreviousPageToken: string;
  currentPageNumber = 1;
  // UI Variables
  @ViewChild('errorDialog', { static: true }) errorDialog: TemplateRef<boolean>;
  infoLoaded = false;
  dataLoaded = false;
  ytChannelImage: string;

  constructor(
    private youtubeService: YoutubeService,
    public loadingService: LoadingService,
    public dialogService: NbDialogService,
    private router: Router,
    private dataSourceBuilder: NbTreeGridDataSourceBuilder<FSEntry>)
    {
      this.getVideosErrorHandler = this.getVideosErrorHandler.bind(this);
      this.getInfoErrorHandler = this.getInfoErrorHandler.bind(this);
    }

  async ngOnInit() {
    await this.getYoutubeChannelInfo();
    await this.getYoutubeVideos();
  }


  //? Getting Videos and Mapping The Result
  async getYoutubeChannelInfo() {
    this.loadingService.showLoader()
    const channelImage: ChannelInfo = await this.youtubeService.getChannelInfo(this.channelID, this.getInfoErrorHandler).toPromise();
    this.ytChannelImage = channelImage.items[0].snippet.thumbnails.medium.url;
    this.infoLoaded = true;
  }

  async getYoutubeVideos(pageToken = '') {
    // Getting Youtube Videos From The Api
    this.youtubeResult = await this.youtubeService.getVideosForChannel(this.channelID, 10, pageToken, this.getVideosErrorHandler).toPromise();

    // Pagination Setup
    this.nextPageToken = this.youtubeResult.nextPageToken;
    this.PreviousPageToken = this.youtubeResult.prevPageToken;

    // Mapping Youtube Api Result To Be Suitable For DataTable "TreeNode<FSEntry>"
    this.data = this.youtubeResult.items.map(function(item,i) {
      return ({ data: { Title: item.snippet.title, Preview: item.snippet.thumbnails.medium.url, Published: formatDate(item.snippet.publishedAt, 'yyyy/MM/dd', 'en'), Details:item.id.videoId } }) as TreeNode<FSEntry>;
    })
    // Building Data Source After Mapping
    this.dataSource = this.dataSourceBuilder.create(this.data);
    this.loadingService.hideLoader();
    this.dataLoaded = true;
  }


  detailsYoutubeVideo(videoID: string) {
    this.router.navigate(['video', videoID]);
  }

  // DataTable Functions
  updateSort(sortRequest: NbSortRequest): void {
    this.sortColumn = sortRequest.column;
    this.sortDirection = sortRequest.direction;
  }

  getSortDirection(column: string): NbSortDirection {
    if (this.sortColumn === column) {
      return this.sortDirection;
    }
    return NbSortDirection.NONE;
  }

  getShowOn(index: number) {
    const minWithForMultipleColumns = 400;
    const nextColumnStep = 100;
    return minWithForMultipleColumns + (nextColumnStep * index);
  }

  nextPage() {
    if (!!this.nextPageToken){
      this.getYoutubeVideos(this.nextPageToken);
      this.currentPageNumber++;
    }
  }

  previousPage() {
    if (!!this.PreviousPageToken){
      this.getYoutubeVideos(this.PreviousPageToken);
      this.currentPageNumber--;
    }
  }


  // Dialogs
  ErrorDialog(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, { context: 'Something Went Wrong' });
  }
  // Error Handlers

  getInfoErrorHandler(error: any) {
    this.loadingService.hideLoader();
    this.ErrorDialog(this.errorDialog);
  }

  getVideosErrorHandler(error: any) {
    this.loadingService.hideLoader();
    this.ErrorDialog(this.errorDialog);
  }


}
