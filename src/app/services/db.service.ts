import { DbModel } from './../models/db.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  private dbSubject: BehaviorSubject<DbModel[]>;
  public db: Observable<DbModel[]>;
  private currentDataStorageKey = 'userData';

  constructor() {
    this.dbSubject = new BehaviorSubject<DbModel[]> (this.parseStoredDb());
    this.db = this.dbSubject.asObservable();
  }

  public get dbValue(): DbModel[] {
    return this.parseStoredDb();
  }

  public saveDbValue(value: DbModel, action: 'fav' | 'rate') {
    const obj = this.dbValue;
    const exist = obj.find(a => a.videoID == value.videoID);
    if (!exist) {
      obj.push(value);
    }
    else {
      if (action === 'fav') {
        exist.isFav = value.isFav;
      }
      if (action === 'rate') {
        exist.rate = value.rate;
      }
    }
    localStorage.setItem(this.currentDataStorageKey, JSON.stringify(obj));
    this.dbSubject.next(obj);
  }

  public getDbValueByID(ID: string) {
    const obj = this.dbValue;
    return obj.find(a => a.videoID == ID)
  }

  private parseStoredDb() {
    const storedDb = localStorage.getItem(this.currentDataStorageKey);
    console.log(storedDb);
    if (storedDb !== undefined && storedDb !== 'undefined') {
      const dbData = JSON.parse(storedDb);
      if (!!dbData) {return dbData;}
    }
    return [];
  }
}
