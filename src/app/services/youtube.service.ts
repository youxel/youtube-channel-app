import { ChannelInfo, VideoInfo, YoutubeSearchModel } from './../models/youtube.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  apiKey : string = 'AIzaSyCVzwHG706gAdBP1ybr50wVF9HKLex9zn0';

  constructor(public http: HttpClient) { }

    getVideosForChannel(channel, maxResults, pageToken = '', errorHandler: any = undefined): Observable<YoutubeSearchModel> {
    let url = 'https://www.googleapis.com/youtube/v3/search?key=' + this.apiKey + '&channelId=' + channel + '&pageToken=' + pageToken + '&order=date&part=snippet &type=video,id&maxResults=' + maxResults
    return this.get(url, errorHandler);
  }

  getChannelInfo(channel, errorHandler: any = undefined): Observable<ChannelInfo> {
    let url = 'https://www.googleapis.com/youtube/v3/channels?part=snippet&id=' + channel + '&fields=items%2Fsnippet%2Fthumbnails&key='+ this.apiKey;
    return this.get(url, errorHandler);
  }

  getVideoInfo(video, errorHandler: any = undefined): Observable<VideoInfo> {
    let url = 'https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=' + video + '&key=' + this.apiKey;
    return this.get(url, errorHandler);
  }

  // Basic HTTP Requests Setup
  private handleError(error: HttpErrorResponse) {
    console.error('API SERVICE ERROR: ', error);
    return throwError('We were unable to connect to the Servers, please check your connection and try again.');
  }

  private get(path: string, errorHandler: any = undefined) {
    return this.call('get', path, errorHandler);
  }

  call(method, path: string, errorHandler: any = undefined) {
    if (!errorHandler) errorHandler = this.handleError;
    // console.debug(`LSG API request '${method}'`, `${ this.apiUrl }/${ path }`, params);
    return this.http
      .request<any>(method, path, {})
      .pipe(
        map((resp) => {
          if (resp.error) {
            console.error('Error', resp);
            throw resp.error;
          }
          // console.log('LSG API response', resp);
          return resp;
        }),
        catchError(errorHandler)
      );
  }
}
