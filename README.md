# Youtube Channel App
**Youtube Channel App** is a standalone offline-storage App written in **AngularX**, YT-Channel-App uses **Youtube API V3** to provide these functionality.
  - Crawling Videos from a channel by ID.
  - Gathering More Details about any Youtube Video.
  - Provides a Basic Rating and Favourite List System.
  - 
![alt text](https://i.imgur.com/azLtho8.png)

# User-Interface
Using **NebularKit** Eva Design System Based which provides Design Assets easily adaptable to any brand. Available for Sketch and Figma component libraries, based on Eva Design System.

# TODO:!
  - Adding **Favourite Videos** Page.
  - Making it more Reliable by including **Channels Directory** Dynamically.
  - Changing Favicon 😅

### Tech
YT-Channel-App uses a number of open source projects to work properly:
* [AngularX] - HTML enhanced for web apps!
* [Visual Studio Codee] - text editor
* [Nebular UI] - great UI KIT for modern web apps
* [Plyr] - a good youtube, vimeo handler
* [Youtube API V3] - Latest Version of Official Youtube API

### Installation

**YT-Channel-App** requires [Angular](https://angular.io/) to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd youtube-channel-app
$ npm install
```

### Serving

```sh
$ ng serve --open
```
